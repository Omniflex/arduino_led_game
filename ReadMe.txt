[CONCEPT]
This is the code for a very simple arduino game made in the fashion of
a peculiar type of casino machine I witnessed while visiting Japan. The contraption
was round and had a long strip of LEDs that went all the way around which lit up
and turned off quickly one after the other. The goal was to press a button exactly
when a specific LED was on. The same concept is implemented here, on a smaller scale.

[GAMEPLAY]
By default, we have three LEDs involved (red, green, red) and the winning one is
the middle one. These LEDs will sequentially flash, and the corresponding will
blink if the button is pressed. If the winning LED is blinking, the game will
speed up, but will slow down otherwise.

[IMPLEMENTATION]
The code is adaptable to choose the number of LEDs and the position of the winning one.
To use the script, just connect the input pin to a button and the output pins
to the LEDs you need. In the current implementation, they need to be consecutively
numbered pins, like 6,7,8. Don't forget the pull-up resistor for the button and
the protective ones for the LEDs and you should be good to go.


[IMPROVEMENTS]
The speed up/slow down process implementation is not that great and could be
largely improved by fixing a framerate for the Arduino and basing the computations
upon it and by carefully adjusting the difficulty curve (purely affine for now).

[LICENSE]
This little project is under GNU GPL, so feel free to use it as you want!

[CONTACT]
omniflex@outlook.fr
