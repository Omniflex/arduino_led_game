int led = 0;
int nb_led = 3-1;
int count = 0;
int lim = 30000;
int input_pin = 2;
int output_pin_offset = 6;
int good_led = 1; //Defines the number of the LED that triggers a success

//Defines a simple blink function for when the button is pressed
void fblink(int pin){
  for (int i=0;i<11;i++){
    digitalWrite(pin, HIGH);
    delay(125);
    digitalWrite(pin, LOW);
    delay(125);
  }
}


void setup() {
  //Pin getting the button input
  pinMode(input_pin,INPUT);
  //Output LED pins must be consecutive numbers
  pinMode(output_pin_offset,OUTPUT);
  pinMode(output_pin_offset+1,OUTPUT);
  pinMode(output_pin_offset+2,OUTPUT);
}


void loop() {

  if (digitalRead(input_pin) == HIGH){
    fblink(led+output_pin_offset);
    if (led == good_led){
      lim = lim - 2500; //Speeds up the game
    }
    else{
      lim = lim + 2500; //Slows down the game
    }
    led = 0; //Starts back to the first LED to prevent cheating
  }

  if (count > lim) { //Select the next LED
    count = 0;
    digitalWrite(led+output_pin_offset, LOW);
    led++;
  }

  if (led > nb_led) {//Selects the first LED after the last
    led = 0;
  }

  digitalWrite(led+output_pin_offset, HIGH); //Lights the selected LED

  count++;

}
